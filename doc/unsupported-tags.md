## Unsupported tags

All tags for _[`cuda-arm64`](https://hub.docker.com/r/nvidia/cuda-arm64/tags) and [`cuda-ppc64le`](https://hub.docker.com/r/nvidia/cuda-ppc64le/tags) are **deprecated**_, and are no longer supported along with the tags listed below.


> :warning: These tags still exist and may contain critical vulnerabilities.<br> 
>   _Use at your own risk._



### ubuntu18.04


#### CUDA 11.0 RC

- `11.0-base-ubuntu18.04-rc`
- `11.0-cudnn8-devel-ubuntu18.04-rc`
- `11.0-cudnn8-runtime-ubuntu18.04-rc`
- `11.0-devel-ubuntu18.04-rc`
- `11.0-runtime-ubuntu18.04-rc`

### ubuntu16.04


#### CUDA 11.0 RC

- `11.0-base-ubuntu16.04-rc`
- `11.0-cudnn8-devel-ubuntu16.04-rc`
- `11.0-cudnn8-runtime-ubuntu16.04-rc`
- `11.0-devel-ubuntu16.04-rc`
- `11.0-runtime-ubuntu16.04-rc`
### centos7
#### CUDA 7.0
- `7.0-cudnn4-devel-centos7`
- `7.0-cudnn4-runtime-centos7`
- `7.0-devel-centos7`
- `7.0-runtime-centos7`
#### CUDA 7.5
- `7.5-cudnn4-devel-centos7`
- `7.5-cudnn4-runtime-centos7`
- `7.5-cudnn5-devel-centos7`
- `7.5-cudnn5-runtime-centos7`
- `7.5-cudnn6-devel-centos7`
- `7.5-cudnn6-runtime-centos7`
- `7.5-devel-centos7`
- `7.5-runtime-centos7`
### ubuntu17.04
#### CUDA 9.1
- `9.1-base-ubuntu17.04`
- `9.1-devel-ubuntu17.04`
- `9.1-runtime-ubuntu17.04`
#### CUDA 9.0
- `9.0-base-ubuntu17.04`
- `9.0-devel-ubuntu17.04`
- `9.0-runtime-ubuntu17.04`
### ubuntu14.04
#### CUDA 10.1
- `10.1-base-ubuntu14.04`
- `10.1-cudnn7-devel-ubuntu14.04`
- `10.1-cudnn7-runtime-ubuntu14.04`
- `10.1-devel-ubuntu14.04`
- `10.1-runtime-ubuntu14.04`
#### CUDA 10.0
- `10.0-base-ubuntu14.04`
- `10.0-cudnn7-devel-ubuntu14.04`
- `10.0-cudnn7-runtime-ubuntu14.04`
- `10.0-devel-ubuntu14.04`
- `10.0-runtime-ubuntu14.04`
#### CUDA 8.0
- `8.0-cudnn5-devel-ubuntu14.04`
- `8.0-cudnn5-runtime-ubuntu14.04`
- `8.0-cudnn6-devel-ubuntu14.04`
- `8.0-cudnn6-runtime-ubuntu14.04`
- `8.0-cudnn7-devel-ubuntu14.04`
- `8.0-cudnn7-runtime-ubuntu14.04`
- `8.0-devel-ubuntu14.04`
- `8.0-runtime-ubuntu14.04`
#### CUDA 6.5
- `6.5-devel-ubuntu14.04`
- `6.5-runtime-ubuntu14.04`
#### CUDA 7.0
- `7.0-cudnn2-devel-ubuntu14.04`
- `7.0-cudnn2-runtime-ubuntu14.04`
- `7.0-cudnn3-devel-ubuntu14.04`
- `7.0-cudnn3-runtime-ubuntu14.04`
- `7.0-cudnn4-devel-ubuntu14.04`
- `7.0-cudnn4-runtime-ubuntu14.04`
- `7.0-devel-ubuntu14.04`
- `7.0-runtime-ubuntu14.04`
#### CUDA 7.5
- `7.5-cudnn3-devel-ubuntu14.04`
- `7.5-cudnn3-runtime-ubuntu14.04`
- `7.5-cudnn4-devel-ubuntu14.04`
- `7.5-cudnn4-runtime-ubuntu14.04`
- `7.5-cudnn5-devel-ubuntu14.04`
- `7.5-cudnn5-runtime-ubuntu14.04`
- `7.5-cudnn6-devel-ubuntu14.04`
- `7.5-cudnn6-runtime-ubuntu14.04`
- `7.5-devel-ubuntu14.04`
- `7.5-runtime-ubuntu14.04`
### centos8
#### CUDA 11.6.0
- `11.6.0-base-centos8`
- `11.6.0-devel-centos8`
- `11.6.0-runtime-centos8`
#### CUDA 11.5.1
- `11.5.1-base-centos8`
- `11.5.1-cudnn8-devel-centos8`
- `11.5.1-cudnn8-runtime-centos8`
- `11.5.1-devel-centos8`
- `11.5.1-runtime-centos8`
#### CUDA 11.5.0
- `11.5.0-base-centos8`
- `11.5.0-cudnn8-devel-centos8`
- `11.5.0-cudnn8-runtime-centos8`
- `11.5.0-devel-centos8`
- `11.5.0-runtime-centos8`
#### CUDA 11.4.3
- `11.4.3-base-centos8`
- `11.4.3-cudnn8-devel-centos8`
- `11.4.3-cudnn8-runtime-centos8`
- `11.4.3-devel-centos8`
- `11.4.3-runtime-centos8`
#### CUDA 11.4.2
- `11.4.2-base-centos8`
- `11.4.2-cudnn8-devel-centos8`
- `11.4.2-cudnn8-runtime-centos8`
- `11.4.2-devel-centos8`
- `11.4.2-runtime-centos8`
#### CUDA 11.4.1
- `11.4.1-base-centos8`
- `11.4.1-cudnn8-devel-centos8`
- `11.4.1-cudnn8-runtime-centos8`
- `11.4.1-devel-centos8`
- `11.4.1-runtime-centos8`
#### CUDA 11.4.0
- `11.4.0-base-centos8`
- `11.4.0-cudnn8-devel-centos8`
- `11.4.0-cudnn8-runtime-centos8`
- `11.4.0-devel-centos8`
- `11.4.0-runtime-centos8`
#### CUDA 11.3.1
- `11.3.1-base-centos8`
- `11.3.1-cudnn8-devel-centos8`
- `11.3.1-cudnn8-runtime-centos8`
- `11.3.1-devel-centos8`
- `11.3.1-runtime-centos8`
#### CUDA 11.3.0
- `11.3.0-base-centos8`
- `11.3.0-cudnn8-devel-centos8`
- `11.3.0-cudnn8-runtime-centos8`
- `11.3.0-devel-centos8`
- `11.3.0-runtime-centos8`
#### CUDA 11.2.2
- `11.2.2-base-centos8`
- `11.2.2-cudnn8-devel-centos8`
- `11.2.2-cudnn8-runtime-centos8`
- `11.2.2-devel-centos8`
- `11.2.2-runtime-centos8`
#### CUDA 11.2.1
- `11.2.1-base-centos8`
- `11.2.1-cudnn8-devel-centos8`
- `11.2.1-cudnn8-runtime-centos8`
- `11.2.1-devel-centos8`
- `11.2.1-runtime-centos8`
#### CUDA 11.2.0
- `11.2.0-base-centos8`
- `11.2.0-cudnn8-devel-centos8`
- `11.2.0-cudnn8-runtime-centos8`
- `11.2.0-devel-centos8`
- `11.2.0-runtime-centos8`
#### CUDA 11.1.1
- `11.1.1-base-centos8`
- `11.1.1-cudnn8-devel-centos8`
- `11.1.1-cudnn8-runtime-centos8`
- `11.1.1-devel-centos8`
- `11.1.1-runtime-centos8`
#### CUDA 11.0.3
- `11.0.3-base-centos8`
- `11.0.3-cudnn8-devel-centos8`
- `11.0.3-cudnn8-runtime-centos8`
- `11.0.3-devel-centos8`
- `11.0.3-runtime-centos8`
#### CUDA 10.2
- `10.2-base-centos8`
- `10.2-cudnn7-devel-centos8`
- `10.2-cudnn7-runtime-centos8`
- `10.2-cudnn8-devel-centos8`
- `10.2-cudnn8-runtime-centos8`
- `10.2-devel-centos8`
- `10.2-runtime-centos8`
#### CUDA 10.1
- `10.1-base-centos8`
- `10.1-cudnn7-devel-centos8`
- `10.1-cudnn7-runtime-centos8`
- `10.1-cudnn8-devel-centos8`
- `10.1-cudnn8-runtime-centos8`
- `10.1-devel-centos8`
- `10.1-runtime-centos8`
#### CUDA 11.0
- `11.0-base-centos8`
- `11.0-cudnn8-devel-centos8`
- `11.0-cudnn8-runtime-centos8`
- `11.0-devel-centos8`
- `11.0-runtime-centos8`
### centos6
#### CUDA 10.2
- `10.2-base-centos6`
- `10.2-cudnn7-devel-centos6`
- `10.2-cudnn7-runtime-centos6`
- `10.2-devel-centos6`
- `10.2-runtime-centos6`
#### CUDA 10.1
- `10.1-base-centos6`
- `10.1-cudnn7-devel-centos6`
- `10.1-cudnn7-runtime-centos6`
- `10.1-devel-centos6`
- `10.1-runtime-centos6`
#### CUDA 10.0
- `10.0-base-centos6`
- `10.0-cudnn7-devel-centos6`
- `10.0-cudnn7-runtime-centos6`
- `10.0-devel-centos6`
- `10.0-runtime-centos6`
#### CUDA 9.2
- `9.2-base-centos6`
- `9.2-cudnn7-devel-centos6`
- `9.2-cudnn7-runtime-centos6`
- `9.2-devel-centos6`
- `9.2-runtime-centos6`
#### CUDA 9.1
- `9.1-base-centos6`
- `9.1-cudnn7-devel-centos6`
- `9.1-cudnn7-runtime-centos6`
- `9.1-devel-centos6`
- `9.1-runtime-centos6`
#### CUDA 9.0
- `9.0-base-centos6`
- `9.0-cudnn7-devel-centos6`
- `9.0-cudnn7-runtime-centos6`
- `9.0-devel-centos6`
- `9.0-runtime-centos6`
#### CUDA 8.0
- `8.0-cudnn5-devel-centos6`
- `8.0-cudnn5-runtime-centos6`
- `8.0-cudnn6-devel-centos6`
- `8.0-cudnn6-runtime-centos6`
- `8.0-cudnn7-devel-centos6`
- `8.0-cudnn7-runtime-centos6`
- `8.0-devel-centos6`
- `8.0-runtime-centos6`
#### CUDA 7.5
- `7.5-cudnn5-devel-centos6`
- `7.5-cudnn5-runtime-centos6`
- `7.5-cudnn6-devel-centos6`
- `7.5-cudnn6-runtime-centos6`
- `7.5-devel-centos6`
- `7.5-runtime-centos6`
